/**
 * Blockly Demos: Code
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview JavaScript for Blockly's Code demo.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

/**
 * Create a namespace for the application.
 */
//var Code = {};
var Code = Code || {};

/**
 * Lookup for names of supported languages.  Keys should be in ISO 639 format.
 */
Code.LANGUAGE_NAME = {
    'ko': '한국어',
    //'en': 'English'
};

/**
 * List of RTL languages.
 */
Code.LANGUAGE_RTL = ['ar', 'fa', 'he', 'lki'];

/**
 * Blockly's main workspace.
 * @type {Blockly.WorkspaceSvg}
 */
Code.workspace = null;

/**
 * Extracts a parameter from the URL.
 * If the parameter is absent default_value is returned.
 * @param {string} name The name of the parameter.
 * @param {string} defaultValue Value to return if paramater not found.
 * @return {string} The parameter value or the default value if not found.
 */
Code.getStringParamFromUrl = function (name, defaultValue) {
    var val = location.search.match(new RegExp('[?&]' + name + '=([^&]+)'));
    return val ? decodeURIComponent(val[1].replace(/\+/g, '%20')) : defaultValue;
};

/**
 * Get the language of this user from the URL.
 * @return {string} User's language.
 */
Code.getLang = function () {
    var lang = Code.getStringParamFromUrl('lang', '');
    if (Code.LANGUAGE_NAME[lang] === undefined) {
        // Default to English.
        lang = 'ko';
    }
    return lang;
};

/**
 * Is the current language (Code.LANG) an RTL language?
 * @return {boolean} True if RTL, false if LTR.
 */
Code.isRtl = function () {
    return Code.LANGUAGE_RTL.indexOf(Code.LANG) != -1;
};


/**
 * Loads an XML file from the server and replaces the current blocks into the
 * Blockly workspace.
 * @param {!string} xmlFile Server location of the XML file to load.
 */
Code.loadServerXmlFile = function(xmlFile) {
  var loadXmlfileAccepted = function() {
    // loadXmlBlockFile loads the file asynchronously and needs a callback
    var loadXmlCb = function(sucess) {
      if (sucess) {
        Code.renderContent();
      }
    };
    Code.loadXmlBlockFile(xmlFile, loadXmlCb);
  };

  if (Code.isWorkspaceEmpty()) {
    loadXmlfileAccepted();
  }
};

/** @return {!boolean} Indicates if the Blockly workspace has blocks. */
Code.isWorkspaceEmpty = function() {
  return Code.workspace.getAllBlocks().length ? false : true;
};

Code.loadXmlBlockFile = function(xmlFile, cbSuccess) {
  var request = Code.ajaxRequest();
  var requestCb = function() {
    if (request.readyState == 4) {
      if (request.status == 200) {
        var success = Code.loadBlocks(request.responseText);
        cbSuccess(success);
      }
    }
  };
  request.open('GET', xmlFile, true);
  request.onreadystatechange = requestCb;
  request.send(null);
};


/** @return {XMLHttpRequest} An XML HTTP Request multi-browser compatible. */
Code.ajaxRequest = function() {
  var request;
  try {
    // Firefox, Chrome, IE7+, Opera, Safari
    request = new XMLHttpRequest();
  } catch (e) {
    try {
      // IE6 and earlier
      request = new ActiveXObject('Msxml2.XMLHTTP');
    } catch (e) {
      try {
        request = new ActiveXObject('Microsoft.XMLHTTP');
      } catch (e) {
        throw 'Your browser does not support AJAX';
        request = null;
      }
    }
  }
  return request;
};


/**
 * Find Block Name
 */

/* Old Function */
function getNameBlock() {
  var blocks = Code.workspace.getTopBlocks(false);
  for (var i = 0, block; block = blocks[i]; i++) {
    if (block.type == 'drone' || block.type == 'inventory') {
      return block;
    }
  }
  return null;
}
/* New Function */
function getNameBlock() {
  var blocks = Code.workspace.getTopBlocks(false);
  var namestring = "";
  for (var i = 0, block; block = blocks[i]; i++) {
    if (block.type == 'drone' || block.type == 'inventory') {
      namestring = namestring.concat(block.getFieldValue('param')) + "&";
    }
  }
  namestring = namestring.slice(0, -1);
  return namestring;
}


/**
 * Save blocks to local file.
 */
Code.savexml = function(filename) {
  var xml = Blockly.Xml.workspaceToDom(Code.workspace);
  var data = Blockly.Xml.domToText(xml);
  // Store data in blob.
  var builder = new BlobBuilder();
  builder.append(data);

  saveAs(builder.getBlob('text/plain;charset=utf-8'), filename + '.xml');

/*
  var nameblock = getNameBlock();
  try {
    saveAs(builder.getBlob('text/plain;charset=utf-8'), nameblock.getFieldValue('param') + '.xml');
  } catch(err) {
    saveAs(builder.getBlob('text/plain;charset=utf-8'), 'untitle.xml');
  }
*/

};


/**
 * Creates an XML file containing the blocks from the Blockly workspace and
 * prompts the users to save it into their local file system.
 */
Code.saveXmlFile = function() {
  Code.savexml(document.getElementById('sketch_name').value);
};


/**
 * Loads an XML file from the users file system and adds the blocks into the
 * Blockly workspace.
 */
Code.loadUserXmlFile = function() {
  // Create File Reader event listener function
  var parseInputXMLfile = function(e) {
    var xmlFile = e.target.files[0];
    var filename = xmlFile.name;
    var extensionPosition = filename.lastIndexOf('.');
    if (extensionPosition !== -1) {
      filename = filename.substr(0, extensionPosition);
    }

    var reader = new FileReader();
    reader.onload = function() {
      var success = Code.replaceBlocksfromXml(reader.result);
      if (success) {
        Code.renderContent();
        Code.sketchNameSet(filename);
      } else {
        Code.alertMessage(
            Code.getLocalStr('invalidXmlTitle'),
            Code.getLocalStr('invalidXmlBody'),
            false);
      }
    };
    reader.readAsText(xmlFile);
  };

  // Create once invisible browse button with event listener, and click it
  var selectFile = document.getElementById('select_file');
  if (selectFile === null) {
    var selectFileDom = document.createElement('INPUT');
    selectFileDom.type = 'file';
    selectFileDom.id = 'select_file';

    var selectFileWrapperDom = document.createElement('DIV');
    selectFileWrapperDom.id = 'select_file_wrapper';
    selectFileWrapperDom.style.display = 'none';
    selectFileWrapperDom.appendChild(selectFileDom);

    document.body.appendChild(selectFileWrapperDom);
    selectFile = document.getElementById('select_file');
    selectFile.addEventListener('change', parseInputXMLfile, false);
  }
  selectFile.click();
};

/**
 * Load blocks from local file.
 */
Code.loadxml = function() {
  var files = event.target.files;
  // Only allow uploading one file.
  if (files.length != 1) {
    return;
  }
  // FileReader
  var reader = new FileReader();
  reader.onloadend = function(event) {
    var target = event.target;
    // 2 == FileReader.DONE
    if (target.readyState == 2) {
      try {
        var xml = Blockly.Xml.textToDom(target.result);
      } catch (e) {
        alert('Error parsing XML:\n' + e);
        return;
      }
      var count = Code.workspace.getAllBlocks().length;
//      if (count && alert('The XML file was not successfully parsed into blocks. \n Please review the XML code and try again.', 'Invalid XML')) {
	if (count && confirm('추가파일을 불러오겠습니까?', 'Load Extra Black')) {
	  //Code.workspace.clear();
      } else { Code.workspace.clear(); }
      //Blockly.Xml.domToWorkspace(Code.workspace, xml);
    }
    // Reset value of input after loading because Chrome will not fire
    // a 'change' event if the same file is loaded again.
    document.getElementById('loadxml').value = '';
  };
  reader.readAsText(files[0]);
};


/**
 * Load blocks saved on App Engine Storage or in session/local storage.
 * @param {string} defaultXml Text representation of default blocks.
 */
Code.loadBlocks = function (defaultXml) {
    try {
        var loadOnce = window.sessionStorage.loadOnceBlocks;
    } catch (e) {
        // Firefox sometimes throws a SecurityError when accessing sessionStorage.
        // Restarting Firefox fixes this, so it looks like a bug.
        var loadOnce = null;
    }
    if ('BlocklyStorage' in window && window.location.hash.length > 1) {
        // An href with #key trigers an AJAX call to retrieve saved blocks.
        BlocklyStorage.retrieveXml(window.location.hash.substring(1));
    } else if (loadOnce) {
        // Language switching stores the blocks during the reload.
        delete window.sessionStorage.loadOnceBlocks;
        var xml = Blockly.Xml.textToDom(loadOnce);
        Blockly.Xml.domToWorkspace(xml, Code.workspace);
    } else if (defaultXml) {
        // Load the editor with default starting blocks.
        var xml = Blockly.Xml.textToDom(defaultXml);
        Blockly.Xml.domToWorkspace(xml, Code.workspace);
    } else if ('BlocklyStorage' in window) {
         //Restore saved blocks in a separate thread so that subsequent
         //initialization is not affected from a failed load.
        window.setTimeout(BlocklyStorage.restoreBlocks, 0);
    }
};

/**
 * Backup code blocks to localStorage.
 */
Code.backup_blocks = function() {
  if ('localStorage' in window) {
    var xml = Blockly.Xml.workspaceToDom(Code.workspace);
    window.localStorage.setItem('blocks', Blockly.Xml.domToText(xml));
  }
}

/**
 * Restore code blocks from localStorage.
 */
Code.restore_blocks = function() {
  if ('localStorage' in window && window.localStorage.blocks) {
    var xml = Blockly.Xml.textToDom(window.localStorage.blocks);
    Blockly.Xml.domToWorkspace(Code.workspace, xml);
  }
}

/**
 * Save the blocks and reload with a different language.
 */
Code.changeLanguage = function () {
    // Store the blocks for the duration of the reload.
    // This should be skipped for the index page, which has no blocks and does
    // not load Blockly.
    // MSIE 11 does not support sessionStorage on file:// URLs.
    if (typeof Blockly != 'undefined' && window.sessionStorage) {
        var xml = Blockly.Xml.workspaceToDom(Code.workspace);
        var text = Blockly.Xml.domToText(xml);
        window.sessionStorage.loadOnceBlocks = text;
    }

    var languageMenu = document.getElementById('languageMenu');
    var newLang = encodeURIComponent(
        languageMenu.options[languageMenu.selectedIndex].value);
    var search = window.location.search;
    if (search.length <= 1) {
        search = '?lang=' + newLang;
    } else if (search.match(/[?&]lang=[^&]*/)) {
        search = search.replace(/([?&]lang=)[^&]*/, '$1' + newLang);
    } else {
        search = search.replace(/\?/, '?lang=' + newLang + '&');
    }

    window.location = window.location.protocol + '//' +
        window.location.host + window.location.pathname + search;
};

/**
 * Bind a function to a button's click event.
 * On touch enabled browsers, ontouchend is treated as equivalent to onclick.
 * @param {!Element|string} el Button element or ID thereof.
 * @param {!Function} func Event handler to bind.
 */
Code.bindClick = function (el, func) {
    if (typeof el == 'string') {
        el = document.getElementById(el);
    }
    el.addEventListener('click', func, true);
    el.addEventListener('touchend', func, true);
};


/**
 * Load the Prettify CSS and JavaScript.
 */
Code.importPrettify = function () {
    //<link rel="stylesheet" href="../prettify.css">
    //<script src="../prettify.js"></script>
    var link = document.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('href', 'google-blockly/demos/prettify.css');
    document.head.appendChild(link);
    var script = document.createElement('script');
    script.setAttribute('src', 'google-blockly/demos/prettify.js');
    document.head.appendChild(script);
};


/**
 * User's language (e.g. "en").
 * @type {string}
 */
Code.LANG = Code.getLang();

/**
 * List of tab names.
 * @private
 */
Code.TABS_ = ['blocks'];

Code.selected = 'blocks';

/**
 * Switch the visible pane when a tab is clicked.
 * @param {string} clickedName Name of tab clicked.
 */
Code.tabClick = function (clickedName) {
    // If the XML tab was open, save and render the content.
    if (document.getElementById('tab_xml').className == 'tabon') {
        var xmlTextarea = document.getElementById('content_xml');
        var xmlText = xmlTextarea.value;
        var xmlDom = null;
        try {
            xmlDom = Blockly.Xml.textToDom(xmlText);
        } catch (e) {
            var q =
                window.confirm(MSG.badXml.replace('%1', e));
            if (!q) {
                // Leave the user on the XML tab.
                return;
            }
        }
        if (xmlDom) {
            Code.workspace.clear();
            Blockly.Xml.domToWorkspace(xmlDom, Code.workspace);
        }
    }

    if (document.getElementById('tab_blocks').className == 'tabon') {
        Code.workspace.setVisible(false);
    }
    // Deselect all tabs and hide all panes.
    for (var i = 0; i < Code.TABS_.length; i++) {
        var name = Code.TABS_[i];
        document.getElementById('tab_' + name).className = 'taboff';
        document.getElementById('content_' + name).style.visibility = 'hidden';
    }

    // Select the active tab.
    Code.selected = clickedName;
    document.getElementById('tab_' + clickedName).className = 'tabon';
    // Show the selected pane.
    document.getElementById('content_' + clickedName).style.visibility =
        'visible';
    Code.renderContent();
    if (clickedName == 'blocks') {
        Code.workspace.setVisible(true);
    }
    Blockly.svgResize(Code.workspace);

    Blockly.addChangeListener(Code.renderCode);
};

/**
 * Populate the currently selected pane with content generated from the blocks.
 */
Code.renderContent = function () {
    var code;
    var content = document.getElementById('content_' + Code.selected);
    // Initialize the pane.
    if (content.id == 'content_xml') {
        var xmlTextarea = document.getElementById('content_xml');
        var xmlDom = Blockly.Xml.workspaceToDom(Code.workspace);
        var xmlText = Blockly.Xml.domToPrettyText(xmlDom);
        xmlTextarea.value = xmlText;
        xmlTextarea.focus();
    } else if (content.id == 'content_javascript') {
        code = Blockly.JavaScript.workspaceToCode(Code.workspace);
        content.textContent = code;
        if (typeof prettyPrintOne == 'function') {
            code = content.innerHTML;
            code = prettyPrintOne(code, 'js');
            content.innerHTML = code;
        }
    }
};

/** @return {!string} Generated Arduino code from the Blockly workspace. */
Code.generateJavascript = function() {
  return Blockly.JavaScript.workspaceToCode(Code.workspace);
};

/** @return {!string} Generated XML code from the Blockly workspace. */
Code.generateXml = function() {
  var xmlDom = Blockly.Xml.workspaceToDom(Code.workspace);
  return Blockly.Xml.domToPrettyText(xmlDom);
};

/**
 * Populate the currently selected pane with content generated from the blocks.
 */
Code.renderCode = function () {

    // Show the selected pane.
    document.getElementById('coll_javascript').style.visibility =
        'visible';

    var code;
    var content = document.getElementById('coll_javascript');
    code = Code.generateJavascript();
    content.textContent = code;
    if (typeof prettyPrintOne == 'function') {
        code = content.innerHTML;
        code = prettyPrintOne(code, 'js');
        content.innerHTML = code;
    }

};

/** Binds functions to each of the buttons, nav links, and related. */
Code.bindActionFunctions = function() {
  // Navigation buttons
  Code.bindClick_('button_load', Code.loadUserXmlFile);
  Code.bindClick_('button_save', Code.saveXmlFile);
  Code.bindClick_('button_delete', Code.discardAllBlocks);

  // Side menu buttons, they also close the side menu
  Code.bindClick_('menu_load', function() {
    Code.loadUserXmlFile();
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_save', function() {
    Code.saveXmlFile();
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_delete', function() {
    Code.discardAllBlocks();
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_settings', function() {
    Code.openSettings();
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_example_hello', function() {
    Code.loadServerXmlFile('../examples/blink.xml');
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_example_2', function() {
    Code.loadServerXmlFile('../examples/serial_print_ascii.xml');
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_example_3', function() {
    Code.loadServerXmlFile('../examples/serial_repeat_game.xml');
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_example_4', function() {
    Code.loadServerXmlFile('../examples/servo_knob.xml');
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick_('menu_example_5', function() {
    Code.loadServerXmlFile('../examples/stepper_knob.xml');
    $('.button-collapse').sideNav('hide');
  });

  // Floating buttons
  Code.bindClick_('button_ide_large', function() {
    Code.ideButtonLargeAction();
  });

};


/**
 * Injects Blockly into a given text area. Reads the toolbox from an XMl file.
 * @param {!Element} blocklyEl Element to inject Blockly into.
 * @param {!string} toolboxPath String containing the toolbox XML file path.
 */
Code.injectBlockly = function(blocklyEl, toolboxPath) {
  var rtl = Code.isRtl();
  // Create a an XML HTTP request
  var request;
  try {   // Firefox, Chrome, IE7+, Opera, Safari
    request = new XMLHttpRequest();
  }
  catch (e) {
    try {   // IE6 and earlier
      request = new ActiveXObject('Msxml2.XMLHTTP');
    }
    catch (e) {
      try {
        request = new ActiveXObject('Microsoft.XMLHTTP');
      }
      catch (e) {
        throw 'Your browser does not support AJAX. Cannot load toolbox';
      }
    }
  }
  request.open('GET', toolboxPath, true);

  // Once file is open, inject blockly into element with the toolbox string
  request.onreadystatechange = function() {
    if ((request.readyState == 4) && (request.status == 200)) {
      Code.workspace = Blockly.inject(blocklyEl, {
          media: 'google-blockly/media/',
          rtl: rtl,
          toolbox: request.responseText,
          collapse: true,
          comments: true,
          css: true,
          disable: true,
          grid: false,
         /*
         grid: {
             spacing: 25,
             length: 3,
             colour: '#ccc',
             snap: true
         },
         */
          maxBlocks: Infinity,
          scrollbars: true,
          sounds: true,
          trashcan: true,
          zoom: {
              controls: true,
              wheel: false,
              startScale: 1.0,
              maxScale: 2,
              minScale: 0.2,
              scaleSpeed: 1.2
          }
      });
    }
  };

  request.send(null);
};


/**
 * Initialize Blockly.  Called on page load.
 */
Code.init = function () {
    Code.initLanguage();

    var rtl = Code.isRtl();

    /*
    var container = document.getElementById('content_area');
    var onresize = function (e) {
        var bBox = Code.getBBox_(container);
        for (var i = 0; i < Code.TABS_.length; i++) {
            var el = document.getElementById('content_' + Code.TABS_[i]);
            el.style.top = bBox.y + 'px';
            el.style.left = bBox.x + 'px';
            // Height and width need to be set, read back, then set again to
            // compensate for scrollbars.
            el.style.height = bBox.height + 'px';
            el.style.height = (2 * bBox.height - el.offsetHeight) + 'px';
            el.style.width = bBox.width + 'px';
            el.style.width = (2 * bBox.width - el.offsetWidth) + 'px';
        }
        // Make the 'Blocks' tab line up with the toolbox.
        if (Code.workspace && Code.workspace.toolbox_.width) {
            document.getElementById('tab_blocks').style.minWidth =
                (Code.workspace.toolbox_.width - 38) + 'px';
            // Account for the 19 pixel margin and on each side.
        }
    };

    window.addEventListener('resize', onresize, false);
    */

   //Code.injectBlockly(
   //    document.getElementById('content_blocks'), 'js/code_toolbox.xml'); 

    
    var toolbox = document.getElementById('toolbox');
    Code.workspace = Blockly.inject('content_blocks', {
	media: 'google-blockly/media/',
        rtl: rtl,
        toolbox: toolbox,

        collapse: true,
        comments: true,
        css: true,
        disable: true,
        grid: false,
        maxBlocks: Infinity,
        scrollbars: true,
        sounds: true,
        trashcan: true,
        zoom: {
            controls: true,
            wheel: false,
            startScale: 1.0,
            maxScale: 2,
            minScale: 0.2,
            scaleSpeed: 1.2
        }
    });
    

    // Add to reserved word list: Local variables in execution environment (runJS)
    // and the infinite loop detection function.
    Blockly.JavaScript.addReservedWords('code,timeouts,checkTimeout');

    /*
    var defaultXml =
      '<xml>' +
      '  <block type="drone" deletable="true">' +
      '  </block>' +
      '</xml>';
    */

    //Code.loadBlocks(defaultXml);
    Code.loadBlocks('');

    Code.designJsInit();
    //Code.bindActionFunctions();

    if ('BlocklyStorage' in window) {
        // Hook a save function onto unload.
        BlocklyStorage.backupOnUnload(Code.workspace);
    }

       // Restore saved blocks in a separate thread so that subsequent
    // initialization is not affected from a failed load.
//    window.setTimeout(Code.restore_blocks, 0);
    // Hook a save function onto unload.
    Blockly.bindEvent_(window, 'unload', null, Code.backup_blocks);

    Code.bindDesignEventListeners();

    Code.tabClick(Code.selected);

    // Init load event.
    /*
    var loadInput = document.getElementById('loadxml');
    loadInput.addEventListener('change', Code.loadxml, false);
    document.getElementById('fakeload').onclick = function() {
      loadInput.click();
    };
    */
    /*
    var loadInput1 = document.getElementById('loadextraxml');
    loadInput.addEventListener('change', Code.loadextraxml, false);
    document.getElementById('fakeload1').onclick = function() {
      loadInput1.click();
    };
    */

  Code.bindClick_('button_load', Code.loadUserXmlFile);


  // Side menu buttons, they also close the side menu
  Code.bindClick_('menu_load', function() {
    Code.loadUserXmlFile();
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick('menu_save', function() {
    Code.saveXmlFile();
    $('.button-collapse').sideNav('hide');
  });
  Code.bindClick('menu_delete', function() {
    Code.discard();
    $('.button-collapse').sideNav('hide');
  });

    Code.bindClick('menu_example_1', function() {
      Code.loadServerXmlFile('examples/hello.xml');
      $('.button-collapse').sideNav('hide');
    });

    Code.bindClick('button_save', Code.saveXmlFile);

    Code.bindClick('trashButton',
        function () {
            Code.discard();
            Code.renderContent();
        });

    Code.bindClick('deployButton', function () {
        var jscode = "var global = this;\n" + Blockly.JavaScript.workspaceToCode(Code.workspace);
        var titleRegexp = /command. '(.+)',/;
		var eventRegexp = /events/;
        var fname = titleRegexp.exec(jscode); //extract the name of the command
		var ename = eventRegexp.exec(jscode); //extract the name of the command
        if (fname === null && ename === null) {
            alert(Blockly.Msg.MISSING_NAME);
        } else {
            //alert(jscode);
            var xhttp;
            if (window.XMLHttpRequest) {
                xhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    alert(Blockly.Msg.DEPLOY_SUCCESS);
                }
            };
            xhttp.open('POST', '/jscode/', true);
            xhttp.send(jscode);
        }
    });
/*
        Code.bindClick('testButton', function () {
        var commandcode = "gamemode 0 @a";
        var xhttp;
        if (window.XMLHttpRequest) {
            xhttp = new XMLHttpRequest();
        } else {
             // code for IE6, IE5
             xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.open('POST', '/button', true);
        xhttp.send(commandcode);
    });
*/

    Code.bindClick('ajax_javascript', function() {
      Code.renderCode();
    });

    // Disable the link button if page isn't backed by App Engine storage.
    /*
    var linkButton = document.getElementById('linkButton');
    if ('BlocklyStorage' in window) {
        BlocklyStorage.HTTPREQUEST_ERROR = MSG.httpRequestError;
        BlocklyStorage.LINK_ALERT = MSG.linkAlert;
        BlocklyStorage.HASH_ERROR = MSG.hashError;
        BlocklyStorage.XML_ERROR = MSG.xmlError;
        Code.bindClick(linkButton,
            function () {
                BlocklyStorage.link(Code.workspace);
            });
    } else if (linkButton) {
        linkButton.className = 'disabled';
    }
    */
    for (var i = 0; i < Code.TABS_.length; i++) {
        var name = Code.TABS_[i];
        Code.bindClick('tab_' + name,
            function (name_) {
                return function () {
                    Code.tabClick(name_);
                };
            } (name));
    }
    //onresize();
    Blockly.svgResize(Code.workspace);

Code.workspace.registerToolboxCategoryCallback(
  'USER_BLOCK', Code.coloursFlyoutCallback);

    // Lazy-load the syntax-highlighting.
//window.setTimeout(Code.importPrettify, 1);
};


/**
 * Initialize the page language.
 */
Code.initLanguage = function () {
    // Set the HTML's language and direction.
    var lang;
    var rtl = Code.isRtl();
    document.dir = rtl ? 'rtl' : 'ltr';
    document.head.parentElement.setAttribute('lang', Code.LANG);

    // Sort languages alphabetically.
    var languages = [];
    for (lang in Code.LANGUAGE_NAME) {
        languages.push([Code.LANGUAGE_NAME[lang], lang]);
    }
    var comp = function (a, b) {
        // Sort based on first argument ('English', 'Русский', '简体字', etc).
        if (a[0] < b[0]) return 1;
        if (a[0] > b[0]) return -1;
        return 0;
    };
    languages.sort(comp);
    // Populate the language selection menu.
    /*
    var languageMenu = document.getElementById('languageMenu');
    languageMenu.options.length = 0;
    for (var i = 0; i < languages.length; i++) {
        var tuple = languages[i];
        lang = tuple[tuple.length - 1];
        var option = new Option(tuple[0], lang);
        if (lang == Code.LANG) {
            option.selected = true;
        }
        languageMenu.options.add(option);
    }
    languageMenu.addEventListener('change', Code.changeLanguage, true);
    */
    // Inject language strings.
    //document.title += ' ' + MSG.title;
    //document.getElementById('title').textContent = MSG.title;
    document.getElementById('tab_blocks').textContent = MSG.blocks;
    document.getElementById('tab_javascript').textContent = MSG.javascript;
    document.getElementById('tab_xml').textContent = MSG.xml;
    //document.getElementById('linkButton').title = MSG.linkTooltip;
    document.getElementById('deployButton').title = MSG.deployTooltip;
    document.getElementById('trashButton').title = MSG.trashTooltip;

/*     var categories = ['catLogic', 'catLoops', 'catMath', 'catText', 'catLists', 'catFunctions',
        'catColour', 'catVariables', 'catDrone', 'catInventory', 'catTeleport', 'catFence', 'catFarmland' , 'catFarming', 'catRail', 'catRiding', 'catVillage', 'catTrampoline', 'catRecipe', 'catRanching', 'catTrap', 'catJukebox', 'catCastle', 'catTag', 'catExample',
		'catHunt', 'catTresure'
    ]; // 모든 카테고리 */
	
	var categories = ['catLogic', 'catLoops', 'catMath', 'catText', 'catLists', 'catFunctions',
        'catVariables', 'catTeleport', 'catFence', 'catFarmland' , 'catRail', 'catJukebox', 'catCastle',
		'catHunt', 'catTresure', 'catVillage', 'catCannon', 'catTunnel', 'catDrone', 'catPlayer', 'catEntity'
    ]; // 수정된 카테고리

    for (var i = 0, cat; cat = categories[i]; i++) {
        document.getElementById(cat).setAttribute('name', MSG[cat]);
    }
    var textVars = document.getElementsByClassName('textVar');
    for (var i = 0, textVar; textVar = textVars[i]; i++) {
        textVar.textContent = MSG.textVariable;
    }
    var listVars = document.getElementsByClassName('listVar');
    for (var i = 0, listVar; listVar = listVars[i]; i++) {
        listVar.textContent = MSG.listVariable;
    }
};

/**
 * Discard all blocks from the workspace.
 */
Code.discard = function () {
    var count = Code.workspace.getAllBlocks().length;
    if (count < 2 ||
        window.confirm(Blockly.Msg.DELETE_ALL_BLOCKS.replace('%1', count))) {
        Code.workspace.clear();
        if (window.location.hash) {
            window.location.hash = '';
        }
    }
};

/**
 * Interface to display messages with a possible action.
 * @param {!string} title HTML to include in title.
 * @param {!element} body HTML to include in body.
 * @param {boolean=} confirm Indicates if the user is shown a single option (ok)
 *     or an option to cancel, with an action applied to the "ok".
 * @param {string=|function=} callback If confirm option is selected this would
 *     be the function called when clicked 'OK'.
 */
Code.alertMessage = function(title, body, confirm, callback) {
  Code.materialAlert(title, body, confirm, callback);
};

Code.bindClick_ = function(el, func) {
  if (typeof el == 'string') {
    el = document.getElementById(el);
  }
  // Need to ensure both, touch and click, events don't fire for the same thing
  var propagateOnce = function(e) {
    e.stopPropagation();
    e.preventDefault();
    func();
  };
  el.addEventListener('ontouchend', propagateOnce);
  el.addEventListener('click', propagateOnce);
};


/**
 * Construct the blocks required by the flyout for the colours category.
 * @param {!Blockly.Workspace} workspace The workspace this flyout is for.
 * @return {!Array.<!Element>} Array of XML block elements.
 */
Code.coloursFlyoutCallback = function(workspace) {
  // Returns an array of hex colours, e.g. ['#4286f4', '#ef0447']
  var xmlList = [];
  if (Blockly.Blocks['teleport_command']) {
      var blockText = '<xml>' +
          '<block type="teleport_command">' +
          '</block>' +
          '</xml>';
      var block = Blockly.Xml.textToDom(blockText).firstChild;
      xmlList.push(block);
  }
  return xmlList;
};


// Load the Code demo's language strings.
//document.write('<script src="js/msg/js/' + Code.LANG + '.js"></script>\n');
// Load Blockly's language strings.
//document.write('<script src="google-blockly/msg/js/' + Code.LANG + '.js"></script>\n');
window.addEventListener('load', Code.init);
