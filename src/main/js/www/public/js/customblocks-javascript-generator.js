/************************************************************************
## Blockly -> ScriptCraft Javascript Code generator
Lauro Canonica: Original author (Devoxx4kids Lugano 2015.04)

Contains the generator for the javascript used in scriptcraft

***/
Blockly.JavaScript['player_chatcommand'] = function(block) { /* 채팅명령어 실행 */
  var chatcommand = Blockly.JavaScript.valueToCode(block, 'chatcommand', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = "player.chat('/jsp '+"+chatcommand+");\n";
  return code;
};

Blockly.JavaScript['player_chat'] = function(block) { /* 채팅창에 말하기 */
  var chat = Blockly.JavaScript.valueToCode(block, 'chat', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = "player.chat("+chat+");\n";
  return code;
};

Blockly.JavaScript['player_location'] = function(block) { // get current drone location
  var code = `player.getLocation()`;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};


Blockly.JavaScript['teleport_command'] = function(block) { /* 텔레포트 함수 */
  var command = Blockly.JavaScript.valueToCode(block, 'command', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `
exports.${command.replace(/'/g, "")} = function(){
  useTeleport();
}
`;
  return code;
};

Blockly.JavaScript['directforward'] = function(block) { /* 숫자만큼 앞으로 이동 명령어 */
  var command = Blockly.JavaScript.valueToCode(block, 'command', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `
exports.${command.replace(/'/g, "")} = function(){
  useForward();
}
`;
  return code;
};

Blockly.JavaScript['directionCommand'] = function(block) { /* 방향보기 명령어 */
  var command = Blockly.JavaScript.valueToCode(block, 'command', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `
exports.${command.replace(/'/g, "")} = function(){
  setDirection();
}
`;
  return code;
};

Blockly.JavaScript['setYaw'] = function(block) { /* 각도 바라보기 */
  var yaw = block.getFieldValue('yaw');

  var code = `setDirection(${yaw});\n`;
  return code;
};

Blockly.JavaScript['setDirection'] = function(block) { /* 방향 바라보기 */
  var yaw = block.getFieldValue('yaw');

  var code = `setDirection(${yaw});\n`;
  return code;
};


/*
 * 리스트
 */
Blockly.JavaScript['list_getindex'] = function(block) { /* 리스트 위치 값 반환 */
  var list = Blockly.JavaScript.valueToCode(block, 'LIST', Blockly.JavaScript.ORDER_ATOMIC);
  var at = Blockly.JavaScript.getAdjusted(block, 'AT');
  // TODO: Assemble JavaScript into code variable.
  var code = list + '[' + at + ']';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['list_setindex'] = function(block) { /* 리스트 위치 값 저장 */
  var list = Blockly.JavaScript.valueToCode(block, 'LIST', Blockly.JavaScript.ORDER_ATOMIC);
  var at = Blockly.JavaScript.getAdjusted(block, 'AT');
  var value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  return list + '[' + at + '] = ' + value + ';\n';
};

/*
 * 문자열
 */
Blockly.JavaScript['string_charAt'] = function(block) { /* 문자 얻기 */
  var value_text = Blockly.JavaScript.valueToCode(block, 'TEXT', Blockly.JavaScript.ORDER_ATOMIC);
  var at = Blockly.JavaScript.getAdjusted(block, 'AT');
  // Adjust index if using one-based indices.
  var code = value_text + '.charAt(' + at + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['string_substring'] = function(block) { /* 문자열 잘라내기 */
  var value_text = Blockly.JavaScript.valueToCode(block, 'TEXT', Blockly.JavaScript.ORDER_ATOMIC);
  var at1 = Blockly.JavaScript.getAdjusted(block, 'AT1');
  var at2 = Blockly.JavaScript.getAdjusted(block, 'AT2', 1);
  // TODO: Assemble JavaScript into code variable.
  code = value_text + '.slice(' + at1 + ', ' + at2 + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

// 텔레포트 이용하기
Blockly.JavaScript['defineCommand'] = function(block) { /* 채팅명령어 */
	var command = block.getFieldValue('command');
	var statements = Blockly.JavaScript.statementToCode(block, 'statements');

  var code = `
exports.${command} = function(){
${statements}}
`;
  return code;
};

Blockly.JavaScript['abosoluteTeleport'] = function(block) {
  var x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
  var y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  var z = Blockly.JavaScript.valueToCode(block, 'z', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `teleport(${x}, ${y}, ${z});\n`
  return code;
};

Blockly.JavaScript['relativeTeleport'] = function(block) {
  var x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
  var y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  var z = Blockly.JavaScript.valueToCode(block, 'z', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `teleport(${x}, ${y}, ${z}, 'relative');\n`
  return code;
};

// 보물찾기
Blockly.JavaScript['forwardPlayer'] = function(block) {
  var distance = Blockly.JavaScript.valueToCode(block, 'distance', Blockly.JavaScript.ORDER_ATOMIC);

	var code = `forward(${distance});\n`
  return code;
};

// 울타리 만들기
Blockly.JavaScript['moveDrone'] = function (block) {
  var direction = block.getFieldValue('direction');

	var code = `drone.${direction};\n`;
  return code;
};

// 사냥하고 돌아오기
Blockly.JavaScript['onEntityDeath'] = function(block) { /* 동물이 죽었을 때 */
  var entity = Blockly.JavaScript.valueToCode(block, 'Mob', Blockly.JavaScript.ORDER_ATOMIC);
  var statements = Blockly.JavaScript.statementToCode(block, 'command');

	var code =
`exports.on${entity}Death = function(){
${statements}}`;
	return code;
};

Blockly.JavaScript['spawnMob'] = function(block) { /* 동물 소환 */
  var age = block.getFieldValue('AGE');
  var mob = Blockly.JavaScript.valueToCode(block, 'animal', Blockly.JavaScript.ORDER_ATOMIC);
  var mob_str = "'"+mob+"'";
  var saddle = block.getFieldValue('SADDLE') == 'TRUE';

  var code = `drone.spawnMob(${mob_str}, ${age}, ${saddle});\n`;
  return code;
};

Blockly.JavaScript['animalmob'] = function(block) { /* 동물 */
  var code = block.getFieldValue('ANIMAL');
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

/*
 * 주크박스 만들기
 */
Blockly.JavaScript['redstoneRepeater'] = function(block) { /* 레드스톤 중계기 */
  var delay = block.getFieldValue('delay');

  var code = `drone.place(${delay});\n`;
  return code;
};

Blockly.JavaScript['moveDroneCount'] = function (block) {
  var direction = block.getFieldValue('direction');
  var count = Blockly.JavaScript.valueToCode(block, 'COUNT', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.${direction}(${count});\n`
  return code;
};

/*
 * 주크박스 만들기
 */
Blockly.JavaScript['placeNote'] = function(block) { /* 소리 블록 */
  var note = Blockly.JavaScript.valueToCode(block, 'note', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.placeNote(${note});\n`;
  return code;
};

Blockly.JavaScript['note'] = function(block) { /* 음 높이 */
  var dropdown_note = block.getFieldValue('note');

  var code = dropdown_note;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

/*
 * 요새 만들기
 */
Blockly.JavaScript['castleRectangle'] = function (block) { /* 요새 직사각형 재료 */
    var width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
    var length = Blockly.JavaScript.valueToCode(block, 'length', Blockly.JavaScript.ORDER_ATOMIC);
    var material = block.getFieldValue('material');
    var fill = block.getFieldValue('fill') == 'TRUE' ? '' : '0';

    var code = `drone.place${fill}(${material},${width},1,${length});\n`
    return code;
};

// DRONE
Blockly.JavaScript['getDroneLocation'] = function(block) { // get current drone location
  var code = `drone.getLocation()`;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['loadDroneLocation'] = function(block) { // Move current drone position to text
  var location = Blockly.JavaScript.valueToCode(block, 'location', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.move(${location});\n`;
  return code;
};

Blockly.JavaScript['materialRectangle'] = function (block) { // 재료 변수로 입력가능
  var width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var length = Blockly.JavaScript.valueToCode(block, 'length', Blockly.JavaScript.ORDER_ATOMIC);
  var material = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);
  var fill = block.getFieldValue('fill') == 'TRUE' ? '' : '0';

  var code = `drone.place${fill}(${material},${width},1,${length});\n`;
  return code;
};

Blockly.JavaScript['materialCuboid'] = function (block) { // 재료 변수로 입력가능
  var width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var length = Blockly.JavaScript.valueToCode(block, 'length', Blockly.JavaScript.ORDER_ATOMIC);
  var height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var material = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);
  var fill = block.getFieldValue('fill') == 'TRUE' ? '' : '0';

  var code = `drone.place${fill}(${material},${width},${height},${length});\n`;
  return code;
};

Blockly.JavaScript['teleportDrone'] = function(block) {
  var x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
  var y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  var z = Blockly.JavaScript.valueToCode(block, 'z', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = `drone.move(${x},${y},${z});\n`;
  return code;
};

Blockly.JavaScript['minihouse'] = function(block) { // make house function
  var width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var length = Blockly.JavaScript.valueToCode(block, 'length', Blockly.JavaScript.ORDER_ATOMIC);
  var height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.minihouse(${width},${length},${height});\n`;
  return code;
};

/*
 * TNT
 */
Blockly.JavaScript['fireTNT'] = function(block) { // TNT발사
  var x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
  var y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  var z = Blockly.JavaScript.valueToCode(block, 'z', Blockly.JavaScript.ORDER_ATOMIC);
  var dir = Blockly.JavaScript.valueToCode(block, 'dir', Blockly.JavaScript.ORDER_ATOMIC);
  var angle = Blockly.JavaScript.valueToCode(block, 'angle', Blockly.JavaScript.ORDER_ATOMIC);
  var power = Blockly.JavaScript.valueToCode(block, 'power', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `fireTNT(${x},${y},${z},${dir},${angle},${power});\n`;
  return code;
};

/*
 * 터널 만들기
 */
Blockly.JavaScript['hang'] = function(block) {
  var hangitem = block.getFieldValue('TYPE');

  var code = `drone.hang(${hangitem});\n`;
  return code;
};

Blockly.JavaScript['drawArc'] = function(block) {
  var blockType = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);
  var rad = Blockly.JavaScript.valueToCode(block, 'radius', Blockly.JavaScript.ORDER_ATOMIC);
  var stroke = Blockly.JavaScript.valueToCode(block, 'strokeWidth', Blockly.JavaScript.ORDER_ATOMIC);
  var stack = Blockly.JavaScript.valueToCode(block, 'stack', Blockly.JavaScript.ORDER_ATOMIC);
  var tl = block.getFieldValue('topleft') == 'TRUE';
  var tr = block.getFieldValue('topright') == 'TRUE';
  var bl = block.getFieldValue('bottomleft') == 'TRUE';
  var br = block.getFieldValue('bottomright') == 'TRUE';
  var orient = block.getFieldValue('orientation');
  var isFill = block.getFieldValue('fill') == 'TRUE';

  var code = `drone.drawArc(${blockType}, ${rad}, ${stroke}, ${tl}, ${tr}, ${bl}, ${br}, '${orient}', ${stack}, ${isFill});\n`;
  return code;
};

Blockly.JavaScript['removeBlock'] = function(block) { // remove block
  var width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var length = Blockly.JavaScript.valueToCode(block, 'length', Blockly.JavaScript.ORDER_ATOMIC);
  var height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.removeBlock(${width}, ${length}, ${height});\n`;
  return code;
};

Blockly.JavaScript['removeXYZ'] = function(block) { // remove block using coordinate
  var x1 = Blockly.JavaScript.valueToCode(block, 'x1', Blockly.JavaScript.ORDER_ATOMIC);
  var y1 = Blockly.JavaScript.valueToCode(block, 'y1', Blockly.JavaScript.ORDER_ATOMIC);
  var z1 = Blockly.JavaScript.valueToCode(block, 'z1', Blockly.JavaScript.ORDER_ATOMIC);
  var x2 = Blockly.JavaScript.valueToCode(block, 'x2', Blockly.JavaScript.ORDER_ATOMIC);
  var y2 = Blockly.JavaScript.valueToCode(block, 'y2', Blockly.JavaScript.ORDER_ATOMIC);
  var z2 = Blockly.JavaScript.valueToCode(block, 'z2', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.removeXYZ(${x1}, ${y1}, ${z1}, ${x2}, ${y2}, ${z2});\n`
  return code;
};

Blockly.JavaScript['setDroneDirection'] = function(block) { // Drone setDirection
  var direction = block.getFieldValue('direction');
  // TODO: Assemble JavaScript into code variable.
  var code = `drone.setDirection('${direction}');\n`;
  return code;
};

Blockly.JavaScript['sphere'] = function (block) { // 재료 변수로 입력가능
  var radius = Blockly.JavaScript.valueToCode(block, 'radius', Blockly.JavaScript.ORDER_ATOMIC);
  var material = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);
  var fill = block.getFieldValue('fill') == 'TRUE' ? '' : '0';

  var code = `drone.sphere${fill}(${material},${radius});\n`;
  return code;
};

Blockly.JavaScript['cylinder'] = function (block) { // 재료 변수로 입력가능
  var radius = Blockly.JavaScript.valueToCode(block, 'radius', Blockly.JavaScript.ORDER_ATOMIC);
  var height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var material = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);
  var fill = block.getFieldValue('fill') == 'TRUE' ? '' : '0';

  var code = `drone.cylinder${fill}(${material},${radius},${height});\n`;
  return code;
};

Blockly.JavaScript['prism'] = function(block) {
  var fill = block.getFieldValue('fill') == 'TRUE' ? '' : '0';
  var width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var length = Blockly.JavaScript.valueToCode(block, 'length', Blockly.JavaScript.ORDER_ATOMIC);
  var material = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.prism${fill}(${material},${width},${length});\n`;
  return code;
};

// Drone Material
Blockly.JavaScript['materialVariable'] = function(block) {
  var material = Blockly.JavaScript.valueToCode(block, 'material', Blockly.JavaScript.ORDER_ATOMIC);

  var code = `drone.place(${material});\n`;
  return code;
};

Blockly.JavaScript['villageMaterial'] = function(block) {
  var material = block.getFieldValue('material');

  var code = material;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['woolMaterial'] = function(block) { // color wool
  var material = block.getFieldValue('material');

  var code = material;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['fenceMaterial'] = function (block) { /* 울타리 재료 */
	var material = block.getFieldValue('material');

  var code = material;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['farmlandMaterial'] = function (block) { /* 밀밭 만들기 재료 */
	var material = block.getFieldValue('material');

  var code = material;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['jukeboxMaterial'] = function (block) { /* 주크박스 재료 */
  var material = block.getFieldValue('material');

  var code = material;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['railMaterial'] = function (block) { /* 롤러코스터 재료 */
    var material = block.getFieldValue('material');

    var code = material;
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['castleMaterial'] = function (block) { /* 요새 재료 */
  var material = block.getFieldValue('material');

  var code = material;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['allMaterial'] = function (block) { /* 모든 재료 */
  var material = block.getFieldValue('material');

  var code = material;
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};
