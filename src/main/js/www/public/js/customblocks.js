/************************************************************************
## Blockly-Minecraft blocks
Lauro Canonica: Original author (Devoxx4kids Lugano 2015.04)

Contains the description of the Minecraft blocks for Blockly

***/

Blockly.BlockSvg.START_HAT = true;

var note = [["낮은 파# (F#)","0"], ["낮은 솔 (G)","1"], ["낮은 솔# (G#)","2"], ["낮은 라 (A)","3"], ["낮은 라# (A#)","4"], ["낮은 시 (B)","5"], ["중간 도 (C)","6"], ["중간 도# (C#)","7"], ["중간 레 (D)","8"], ["중간 레# (D#)","9"], ["중간 미 (E)","10"], ["중간 파 (F)","11"], ["중간 파# (F#)","12"], ["중간 솔 (G)","13"], ["중간 솔# (G#)","14"], ["중간 라 (A)","15"], ["중간 라# (A#)","16"], ["중간 시 (B)","17"], ["높은 도 (C)","18"], ["높은 도# (C#)","19"], ["높은 레 (D)","20"], ["높은 레# (D#)","21"], ["높은 미 (E)","22"], ["높은 파 (F)","23"], ["높은 파# (F#)","24"]];
var fence = getNames(Blockly.Msg.OBJNAMES, ['blocks.fence.oak', 'blocks.fence.spruce', 'blocks.fence.birch', 'blocks.fence.jungle', 'blocks.fence.dark_oak', 'blocks.fence.acacia',
'blocks.gate.oak', 'blocks.gate.spruce', 'blocks.gate.birch', 'blocks.gate.jungle', 'blocks.gate.dark_oak', 'blocks.gate.acacia', 'blocks.air']);

var farmland = getNames(Blockly.Msg.OBJNAMES, ['blocks.water', 'blocks.farmland', 'blocks.wheat_seeds', 'blocks.beetroot', 'blocks.potatoes', 'blocks.carrots', 'blocks.air']);

var jukebox = getNames(Blockly.Msg.OBJNAMES, ['blocks.redstone_wire', 'blocks.stone', 'blocks.pressure_plate_stone', 'blocks.air']);

var rail = getNames(Blockly.Msg.OBJNAMES, ['blocks.rail', 'blocks.powered_rail', 'blocks.detector_rail', 'blocks.rail_activator', 'blocks.redstone', 'blocks.stone', 'blocks.air']);

var castle = getNames(Blockly.Msg.OBJNAMES, ['blocks.brick.stone', 'blocks.ice', 'blocks.glass', 'blocks.brick.red', 'blocks.oak', 'blocks.air']);

var village = getNames(Blockly.Msg.OBJNAMES, ['blocks.brick.stone', 'blocks.dirt', 'blocks.oak', 'blocks.brick.red', 'blocks.glass', 'blocks.ice', 'blocks.air', 'blocks.water',
'blocks.farmland', 'blocks.wheat_seeds', 'blocks.beetroot', 'blocks.potatoes', 'blocks.carrots' , 'blocks.flower_yellow','blocks.flower.red', 'blocks.flower.blueorchid',
'blocks.flower.allium', 'blocks.flower.azure_bluet', 'blocks.flower.red_tulip', 'blocks.flower.orange_tulip', 'blocks.flower.white_tulip', 'blocks.flower.pink_tulip',
'blocks.flower.oxeye_daisy', 'blocks.grass_path', 'blocks.sapling.oak', 'blocks.sapling.spruce', 'blocks.sapling.birch', 'blocks.sapling.jungle', 'blocks.sapling.acacia',
'blocks.wool.white', 'blocks.wool.orange', 'blocks.wool.magenta', 'blocks.wool.lightblue', 'blocks.wool.yellow', 'blocks.wool.lime', 'blocks.wool.pink', 'blocks.wool.gray',
'blocks.wool.lightgray', 'blocks.wool.cyan', 'blocks.wool.purple', 'blocks.wool.blue', 'blocks.wool.brown', 'blocks.wool.green', 'blocks.wool.red', 'blocks.wool.black']);

var wool = getNames(Blockly.Msg.OBJNAMES, ['blocks.wool.white', 'blocks.wool.orange', 'blocks.wool.magenta', 'blocks.wool.lightblue', 'blocks.wool.yellow', 'blocks.wool.lime',
'blocks.wool.pink', 'blocks.wool.gray', 'blocks.wool.lightgray', 'blocks.wool.cyan', 'blocks.wool.purple', 'blocks.wool.blue', 'blocks.wool.brown', 'blocks.wool.green',
'blocks.wool.red', 'blocks.wool.black']);

var allMaterial = getNames(Blockly.Msg.OBJNAMES, ['blocks.stone', 'blocks.air', 'blocks.dirt', 'blocks.fence.oak', 'blocks.fence.spruce', 'blocks.fence.birch', 'blocks.fence.jungle',
'blocks.fence.dark_oak', 'blocks.fence.acacia', 'blocks.gate.oak', 'blocks.gate.spruce', 'blocks.gate.birch', 'blocks.gate.jungle', 'blocks.gate.dark_oak', 'blocks.gate.acacia',
'blocks.water', 'blocks.farmland', 'blocks.wheat_seeds', 'blocks.beetroot', 'blocks.potatoes', 'blocks.carrots','blocks.redstone_wire', 'blocks.pressure_plate_stone',
'blocks.rail', 'blocks.powered_rail', 'blocks.detector_rail', 'blocks.rail_activator', 'blocks.redstone', 'blocks.brick.stone', 'blocks.ice', 'blocks.glass', 'blocks.brick.red',
'blocks.oak', 'blocks.flower_yellow','blocks.flower.red', 'blocks.flower.blueorchid',
'blocks.flower.allium', 'blocks.flower.azure_bluet', 'blocks.flower.red_tulip', 'blocks.flower.orange_tulip', 'blocks.flower.white_tulip', 'blocks.flower.pink_tulip',
'blocks.flower.oxeye_daisy', 'blocks.grass_path', 'blocks.sapling.oak', 'blocks.sapling.spruce', 'blocks.sapling.birch', 'blocks.sapling.jungle', 'blocks.sapling.acacia',
'blocks.wool.white', 'blocks.wool.orange', 'blocks.wool.magenta', 'blocks.wool.lightblue', 'blocks.wool.yellow', 'blocks.wool.lime', 'blocks.wool.pink', 'blocks.wool.gray',
'blocks.wool.lightgray', 'blocks.wool.cyan', 'blocks.wool.purple', 'blocks.wool.blue', 'blocks.wool.brown', 'blocks.wool.green', 'blocks.wool.red', 'blocks.wool.black']);

function getNames(list, blockObjs){
  var output = [];
  var blockMeta = [];
  var displayName = '';
  for(i = 0; i < blockObjs.length; i++){
    blockMeta = blockObjs[i].split('.');
    if(blockMeta.length == 3){
      displayName = list[blocks[blockMeta[1]][blockMeta[2]]];
    } else { // blockMeta.length == 2
      displayName = list[blocks[blockMeta[1]]];
    }
    output[i] = [displayName, blockObjs[i]];
  }
  return output;
}

Blockly.Blocks['player_chatcommand'] = { /* 채팅명령어 실행 */
  init: function() {
    this.appendValueInput("chatcommand")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("명령어 실행");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("사용자의 코드로 채팅 명령을 실행합니다.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['player_chat'] = { /* 채팅창에 말하기 */
  init: function() {
    this.appendValueInput("chat")
        .setCheck(["Number", "String", "Boolean", "Array"])
        .appendField("채팅창에 말하기 :");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("게임 채팅에 메시지를 기록합니다.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['player_location'] = { // get player current location
  init: function() {
    this.appendDummyInput()
        .appendField("플레이어의 현재 위치");
    this.setOutput(true, null);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['teleport_command'] = { /* 텔레포트 함수 */
  init: function() {
    this.appendValueInput("command")
        .setCheck(null)
        .appendField("좌표로 텔레포트 명령어");
    this.appendDummyInput()
        .appendField("X Y Z");
    this.setInputsInline(true);
    this.setColour(210);
    this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['directforward'] = { /* 숫자만큼 앞으로 이동 명령어 */
  init: function() {
    this.appendValueInput("command")
        .setCheck(null)
        .appendField("앞으로 이동 명령어");
    this.appendDummyInput()
        .appendField("거리");
    this.setInputsInline(true);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['directionCommand'] = { /* 숫자만큼 앞으로 이동 명령어 */
  init: function() {
    this.appendValueInput("command")
        .setCheck(null)
        .appendField("방향보기 명령어");
    this.appendDummyInput()
        .appendField("방향");
    this.setInputsInline(true);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['setYaw'] = { /* 각도 바라보기 */
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldAngle(90), "yaw");
    this.appendDummyInput()
        .appendField("방향 보기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['setDirection'] = { /* 방향 바라보기 */
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["동쪽 (270º)","270"], ["서쪽 (90º)","90"], ["남쪽 (0º)","0"], ["북쪽 (180º)","180"]]), "yaw");
    this.appendDummyInput()
        .appendField("방향 보기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

/*
 * 울타리 만들기
 */
Blockly.Blocks['fence_material'] = { /* 울타리 재료 */
    init: function () {
        this.appendDummyInput()
            .appendField(Blockly.Msg.MATERIALS)
            .appendField(new Blockly.FieldDropdown(fence), "material");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(0);
        this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
        this.setHelpUrl('http://minecraft.gamepedia.com/Block');
    }
};

/*
 * 리스트
 */
 Blockly.Blocks['list_getindex'] = { /* 리스트 위치 값 반환 */
  init: function() {
    this.appendValueInput("LIST")
        .setCheck("Array");
    this.appendValueInput("AT")
        .setCheck("Number")
        .appendField("리스트의");
    this.appendDummyInput()
        .appendField("번째 값");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(260);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['list_setindex'] = { /* 리스트 위치 값 저장 */
  init: function() {
    this.appendValueInput("LIST")
        .setCheck("Array");
    this.appendValueInput("AT")
        .setCheck("Number")
        .appendField("리스트의");
    this.appendValueInput("VALUE")
        .setCheck(null)
        .appendField("번째 값에");
    this.appendDummyInput()
        .appendField("저장");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(260);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

/*
 * 문자열
 */
 Blockly.Blocks['string_charAt'] = { /* 문자 얻기 */
  init: function() {
    this.appendValueInput("TEXT")
        .setCheck(null)
        .appendField("문자열");
    this.appendValueInput("AT")
        .setCheck(null)
		.appendField("의");
    this.appendDummyInput()
        .appendField("번 째 문자");
    this.setOutput(true, null);
    this.setColour(160);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['string_substring'] = { /* 문자열 잘라내기 */
  init: function() {
    this.appendValueInput("TEXT")
        .setCheck(null)
        .appendField("문자열");
    this.appendValueInput("AT1")
        .setCheck(null)
    this.appendValueInput("AT2")
        .setCheck(null)
        .appendField("번 째부터");
    this.appendDummyInput()
        .appendField("번 째까지 잘라내기");
    this.setOutput(true, null);
    this.setColour(160);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

// 텔레포트 이용하기
Blockly.Blocks['defineCommand'] = { /* 채팅명령어 */
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldTextInput("run"), "command")
        .appendField("를 입력하면 실행");
    this.appendStatementInput("statements")
        .setCheck(null);
    this.setInputsInline(true);
    this.setColour(210);
 this.setTooltip("채팅으로 코드를 실행합니다.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['abosoluteTeleport'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("플레이어를");
    this.appendValueInput("x")
        .setCheck(null)
        .appendField("X");
    this.appendValueInput("y")
        .setCheck(null)
        .appendField("Y");
    this.appendValueInput("z")
        .setCheck(null)
        .appendField("Z");
    this.appendDummyInput()
        .appendField("로 텔레포트");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['relativeTeleport'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("플레이어를");
    this.appendValueInput("x")
        .setCheck(null)
        .appendField("~X");
    this.appendValueInput("y")
        .setCheck(null)
        .appendField("~Y");
    this.appendValueInput("z")
        .setCheck(null)
        .appendField("~Z");
    this.appendDummyInput()
        .appendField("로 텔레포트");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

// 보물찾기
Blockly.Blocks['forwardPlayer'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("플레이어를");
    this.appendValueInput("distance")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("만큼 앞으로 이동");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(210);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

// 울타리 만들기
Blockly.Blocks['moveDrone'] = {
    init: function () {
      this.appendDummyInput()
          .appendField("드론을")
          .appendField(
          new Blockly.FieldDropdown([
              [Blockly.Msg.MOUVEMENT_FWD+" 이동", "forward()"],
              [Blockly.Msg.MOUVEMENT_BACK+" 이동", "back()"],
              [Blockly.Msg.MOUVEMENT_UP+" 이동", "up()"],
              [Blockly.Msg.MOUVEMENT_DOWN+" 이동", "down()"],
              [Blockly.Msg.MOUVEMENT_LEFT+" 이동", "left()"],
              [Blockly.Msg.MOUVEMENT_RIGHT+" 이동", "right()"],
              [Blockly.Msg.MOUVEMENT_TURN_LEFT, "turnLeft()"],
              [Blockly.Msg.MOUVEMENT_TURN_RIGHT, "turnRight()"],
          		[Blockly.Msg.MOUVEMENT_TURN_BACK, "turnBack()"],
              [Blockly.Msg.MOUVEMENT_SAVESTART, "checkpoint('start')"],
          		[Blockly.Msg.MOUVEMENT_BACKTOSTART, "move('start')"]
          ]), "direction");
      this.setInputsInline(true);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setColour(0);
      this.setTooltip(Blockly.Msg.TOOLTIP_DRONEMOVE);
      this.setHelpUrl('https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md#drone-movement');
    }
};

// 사냥하고 돌아오기
Blockly.Blocks['onEntityDeath'] = { /* 동물이 죽었을 때 */
  init: function() {
    this.appendValueInput("Mob")
        .setCheck("AnimalMob");
    this.appendDummyInput()
        .appendField("이 죽으면 실행");
    this.appendStatementInput("command")
        .setCheck(null);
    this.setInputsInline(true);
    this.setColour(230);
 this.setTooltip("특정 유형의 생명체를 죽이면 코드를 실행합니다.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['spawnMob'] = {  /* 동물 소환 */
  init: function() {
    this.appendDummyInput()
        .appendField(" ")
        .appendField(new Blockly.FieldDropdown([["어른","true"], ["새끼","false"]]), "AGE");
    this.appendValueInput("animal")
        .appendField(" ")
        .setCheck("AnimalMob");
    this.appendDummyInput()
        .appendField("소환   ")
        .appendField(new Blockly.FieldCheckbox("FALSE"), "SADDLE");
    this.appendDummyInput()
        .appendField("안장");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("동물을 생성합니다.");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['animalmob'] = { /* 동물 */
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["닭","CHICKEN"], ["소","COW"], ["돼지","PIG"], ["양","SHEEP"], ["늑대","WOLF"],["말","HORSE"]]), "ANIMAL");
    this.setOutput(true, "AnimalMob");
    this.setColour(230);
 this.setTooltip("게임에서 동물을 나타냅니다.");
 this.setHelpUrl("");
  }
};

/*
 * 주크박스 만들기
 */

Blockly.Blocks['redstoneRepeater'] = { /* 레드스톤 중계기 */
  init: function() {
    this.appendDummyInput()
        .appendField("드론으로 지연시간이 ")
        .appendField(new Blockly.FieldDropdown([["1",'blocks.repeater.delay_1'], ["2",'blocks.repeater.delay_2'], ["3",'blocks.repeater.delay_3'], ["4",'blocks.repeater.delay_4']]), "delay")
        .appendField("인 중계기 놓기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['moveDroneCount'] = {
    init: function () {
      this.appendDummyInput()
          .appendField("드론을")
            .appendField(
            new Blockly.FieldDropdown([
                [Blockly.Msg.MOUVEMENT_UP, "up"],
                [Blockly.Msg.MOUVEMENT_DOWN, "down"],
                [Blockly.Msg.MOUVEMENT_FWD, "fwd"],
                [Blockly.Msg.MOUVEMENT_BACK, "back"],
                [Blockly.Msg.MOUVEMENT_RIGHT, "right"],
                [Blockly.Msg.MOUVEMENT_LEFT, "left"]
            ]), "direction");
        this.appendValueInput("COUNT")
			.setCheck(null)
			.appendField(" ");
		this.appendDummyInput()
			.appendField("칸 이동");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(0);
        this.setTooltip(Blockly.Msg.TOOLTIP_DRONEMOVE);
        this.setHelpUrl('https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md#drone-movement');
    }
};

Blockly.Blocks['placeNote'] = { /* 노트 블록 */
    init: function () {
      this.appendDummyInput()
          .appendField("드론으로");
  		this.appendValueInput("note")
      		.setCheck(null);
      this.appendDummyInput()
          .appendField("소리 블록 놓기");
      this.setInputsInline(true);
      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setColour(0);
      this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
      this.setHelpUrl('http://minecraft.gamepedia.com/Block');
    }
};

Blockly.Blocks['note'] = { /* 음 높이 */
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(note), "note");
    this.setOutput(true, null);
    this.setColour(0);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

/*
 * 요새 만들기
 */
Blockly.Blocks['castleRectangle'] = { /* 요새 직사각형 재료 */
    init: function () {
      this.appendValueInput("width").setCheck("Number")
          .appendField(Blockly.Msg.WIDTH);
      this.appendValueInput("length").setCheck("Number")
          .appendField(Blockly.Msg.LENGTH);
      this.appendDummyInput()
          .appendField("크기의 ")
          .appendField(new Blockly.FieldDropdown(castle), "material");
        this.appendDummyInput()
            .appendField(Blockly.Msg.RECTANGLE + " 만들기  ");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("TRUE"), "fill");
        this.appendDummyInput()
            .appendField("채우기");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(0);
        this.setTooltip(Blockly.Msg.TOOLTIP_RECTANGLE);
        this.setHelpUrl('https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md#dronebox-method');
    }
};

// DRONE

Blockly.Blocks['getDroneLocation'] = { // get current drone location
  init: function() {
    this.appendDummyInput()
        .appendField("드론의 현재 위치");
    this.setOutput(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['loadDroneLocation'] = { // Move current drone position to text
  init: function() {
    this.appendValueInput("location")
        .setCheck(null)
        .appendField("현재 드론 위치를");
    this.appendDummyInput()
        .appendField("로 이동");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['materialRectangle'] = { // 재료값 변수로 입력가능
    init: function () {
      this.appendValueInput("width").setCheck("Number")
          .appendField(Blockly.Msg.WIDTH);
      this.appendValueInput("length").setCheck("Number")
          .appendField(Blockly.Msg.LENGTH);
      this.appendDummyInput()
          .appendField("크기의 ");
      this.appendValueInput("material")
        .setCheck(null);
      this.appendDummyInput()
          .appendField(Blockly.Msg.RECTANGLE + " 만들기  ");
      this.appendDummyInput()
          .appendField(new Blockly.FieldCheckbox("TRUE"), "fill");
      this.appendDummyInput()
          .appendField("채우기");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(0);
        this.setTooltip(Blockly.Msg.TOOLTIP_RECTANGLE);
        this.setHelpUrl('https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md#dronebox-method');
    }
};

Blockly.Blocks['materialCuboid'] = { // 재료값 변수로 입력가능
    init: function () {
      this.appendValueInput("width").setCheck("Number")
          .appendField(Blockly.Msg.WIDTH);
      this.appendValueInput("length").setCheck("Number")
          .appendField(Blockly.Msg.LENGTH);
      this.appendValueInput("height").setCheck("Number")
          .appendField(Blockly.Msg.HEIGHT);
      this.appendDummyInput()
          .appendField("크기의 ");
      this.appendValueInput("material")
        .setCheck(null);
      this.appendDummyInput()
          .appendField("직육면체 만들기  ");
      this.appendDummyInput()
          .appendField(new Blockly.FieldCheckbox("TRUE"), "fill");
      this.appendDummyInput()
          .appendField("채우기");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(0);
        this.setTooltip(Blockly.Msg.TOOLTIP_RECTANGLE);
        this.setHelpUrl('https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md#dronebox-method');
    }
};

Blockly.Blocks['teleportDrone'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("드론을");
    this.appendValueInput("x")
        .setCheck("Number")
        .appendField("X");
    this.appendValueInput("y")
        .setCheck("Number")
        .appendField("Y");
    this.appendValueInput("z")
        .setCheck("Number")
        .appendField("Z");
    this.appendDummyInput()
        .appendField("좌표로 이동");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['minihouse'] = { // make house function
  init: function() {
    this.appendDummyInput()
        .appendField("드론으로");
    this.appendValueInput("width")
        .setCheck(null)
        .appendField("가로");
    this.appendValueInput("length")
        .setCheck(null)
        .appendField("세로");
    this.appendValueInput("height")
        .setCheck(null)
        .appendField("높이");
    this.appendDummyInput()
        .appendField("집 만들기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

/*
 * TNT
 */
Blockly.Blocks['fireTNT'] = { // TNT발사
  init: function() {
    this.appendValueInput("x")
        .setCheck("Number")
        .appendField("TNT ")
        .appendField("x");
    this.appendValueInput("y")
        .setCheck("Number")
        .appendField("y");
    this.appendValueInput("z")
        .setCheck("Number")
        .appendField("z");
    this.appendValueInput("dir")
        .setCheck("Number")
        .appendField("방향");
    this.appendValueInput("angle")
        .setCheck("Number")
        .appendField("° 각도");
    this.appendDummyInput()
        .appendField("°");
    this.appendValueInput("power")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField("의 힘으로 발사");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};


/*
 * 터널 만들기
 */
Blockly.Blocks['hang'] = { /*블록옆에 매달리는 아이템*/
  init: function() {
    this.appendDummyInput()
        .appendField("드론으로 ");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["횃불",'blocks.torch'], ["레드스톤횃불",'blocks.torch_redstone_active'], ["트립와이어 후크",'blocks.tripwire_hook']]), "TYPE");
    this.appendDummyInput()
        .appendField("매달기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['drawArc'] = {
  init: function() {
    this.appendValueInput("radius")
        .setCheck(null)
        .appendField("반지름");
    this.appendValueInput("strokeWidth")
        .setCheck(null)
        .appendField("선폭");
    this.appendValueInput("stack")
        .setCheck(null)
        .appendField("두께");
    this.appendValueInput("material")
        .appendField("인")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("원을 ")
        .appendField(new Blockly.FieldDropdown([["수직으로","vertical"], ["수평으로","horizontal"]]), "orientation");
    this.appendDummyInput()
        .appendField("만들기  ");
    this.appendDummyInput()
        .appendField(new Blockly.FieldCheckbox("TRUE"), "topleft")
        .appendField("좌상  ")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "topright")
        .appendField("우상  ")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "bottomleft")
        .appendField("좌하  ")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "bottomright")
        .appendField("우하  ")
        .appendField(new Blockly.FieldCheckbox("TRUE"), "fill")
        .appendField("채우기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['removeBlock'] = { // remove block
  init: function() {
    this.appendValueInput("width")
        .setCheck("Number")
        .appendField("가로");
    this.appendValueInput("length")
        .setCheck("Number")
        .appendField("세로");
    this.appendValueInput("height")
        .setCheck("Number")
        .appendField("높이");
    this.appendDummyInput()
        .appendField("만큼 블록 제거하기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['removeXYZ'] = { // remove block using coordinate
  init: function() {
    this.appendValueInput("x1")
        .setCheck("Number")
        .appendField("x1");
    this.appendValueInput("y1")
        .setCheck("Number")
        .appendField("y1");
    this.appendValueInput("z1")
        .setCheck("Number")
        .appendField("z1");
    this.appendValueInput("x2")
        .setCheck("Number")
        .appendField("부터   x2");
    this.appendValueInput("y2")
        .setCheck("Number")
        .appendField("y2");
    this.appendValueInput("z2")
        .setCheck("Number")
        .appendField("z2");
    this.appendDummyInput()
        .appendField("까지 블록 제거하기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['setDroneDirection'] = { // Drone setDirection
  init: function() {
    this.appendDummyInput()
        .appendField("드론을")
        .appendField(new Blockly.FieldDropdown([["동쪽",'east'], ["서쪽",'west'], ["남쪽",'south'], ["북쪽",'north']]), "direction")
        .appendField("방향으로 회전");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['sphere'] = { // 재료값 변수로 입력가능
    init: function () {
      this.appendValueInput("radius").setCheck("Number")
          .appendField("반지름이");
      this.appendDummyInput()
          .appendField("인 ");
      this.appendValueInput("material")
          .setCheck(null);
      this.appendDummyInput()
          .appendField("구 만들기  ");
      this.appendDummyInput()
          .appendField(new Blockly.FieldCheckbox("TRUE"), "fill");
      this.appendDummyInput()
          .appendField("채우기");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(0);
        this.setTooltip(Blockly.Msg.TOOLTIP_RECTANGLE);
        this.setHelpUrl('https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md#dronebox-method');
    }
};

Blockly.Blocks['cylinder'] = { // 재료값 변수로 입력가능
    init: function () {
      this.appendValueInput("radius").setCheck("Number")
          .appendField("반지름");
      this.appendValueInput("height").setCheck("Number")
          .appendField("높이");
      this.appendDummyInput()
          .appendField("인 ");
      this.appendValueInput("material")
          .setCheck(null);
      this.appendDummyInput()
          .appendField("원통 만들기  ");
      this.appendDummyInput()
          .appendField(new Blockly.FieldCheckbox("TRUE"), "fill");
      this.appendDummyInput()
          .appendField("채우기");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(0);
        this.setTooltip(Blockly.Msg.TOOLTIP_RECTANGLE);
        this.setHelpUrl('https://github.com/walterhiggins/ScriptCraft/blob/master/docs/API-Reference.md#dronebox-method');
    }
};

Blockly.Blocks['prism'] = {
  init: function() {
    this.appendValueInput("width")
        .setCheck(null)
        .appendField(Blockly.Msg.WIDTH);
    this.appendValueInput("length")
        .setCheck(null)
        .appendField(Blockly.Msg.LENGTH);
    this.appendValueInput("material")
    	   .appendField("크기의")
         .setCheck(null);
    this.appendDummyInput()
        .appendField("지붕 만들기  ");
    this.appendDummyInput()
        .appendField(new Blockly.FieldCheckbox("TRUE"), "fill");
    this.appendDummyInput()
        .appendField("채우기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};


// Drone Material
Blockly.Blocks['materialVariable'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("드론으로");
    this.appendValueInput("material")
        .setCheck(null);
      this.appendDummyInput()
          .appendField("놓기");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['villageMaterial'] = { // 마을재료
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(village), "material");
    this.setOutput(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};


Blockly.Blocks['woolMaterial'] = { // color wool
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(wool), "material");
    this.setOutput(true, null);
    this.setColour(0);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['fenceMaterial'] = { /* 울타리 재료 */
    init: function () {
      this.appendDummyInput()
          .appendField(new Blockly.FieldDropdown(fence), "material");
      this.setOutput(true, null);
      this.setColour(0);
      this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
      this.setHelpUrl('http://minecraft.gamepedia.com/Block');
    }
};

Blockly.Blocks['farmlandMaterial'] = { /* 밀밭 만들기 재료 */
  init: function () {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(farmland), "material");
    this.setOutput(true, null);
    this.setColour(0);
    this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
    this.setHelpUrl('http://minecraft.gamepedia.com/Block');
  }
};

Blockly.Blocks['jukeboxMaterial'] = { /* 주크박스 재료 */
    init: function () {
      this.appendDummyInput()
          .appendField(new Blockly.FieldDropdown(jukebox), "material");
      this.setOutput(true, null);
      this.setColour(0);
      this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
      this.setHelpUrl('http://minecraft.gamepedia.com/Block');
    }
};

Blockly.Blocks['railMaterial'] = { /* 롤러코스터 재료 */
    init: function () {
      this.appendDummyInput()
          .appendField(new Blockly.FieldDropdown(rail), "material");
      this.setOutput(true, null);
      this.setColour(0);
      this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
      this.setHelpUrl('http://minecraft.gamepedia.com/Block');
    }
};

Blockly.Blocks['castleMaterial'] = { /* 요새 재료 */
    init: function () {
      this.appendDummyInput()
          .appendField(new Blockly.FieldDropdown(castle), "material");
      this.setOutput(true, null);
      this.setColour(0);
      this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
      this.setHelpUrl('http://minecraft.gamepedia.com/Block');
    }
};

Blockly.Blocks['allMaterial'] = { /* 모든 재료 */
    init: function () {
      this.appendDummyInput()
          .appendField(new Blockly.FieldDropdown(allMaterial), "material");
      this.setOutput(true, null);
      this.setColour(0);
      this.setTooltip(Blockly.Msg.TOOLTIP_MATERIALS);
      this.setHelpUrl('http://minecraft.gamepedia.com/Block');
    }
};
