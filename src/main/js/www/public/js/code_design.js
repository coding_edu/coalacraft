/**
 * Blockly Demos: Code
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview JavaScript for Blockly's Code demo.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

/**
 * Create a namespace for the application.
 */
var Code = Code || {};

Code.designJsInit = function() {
  Code.materializeJsInit();
  Code.sketchNameSizeEffect();
  Code.sketchNameSet();
}


/**
 * Initialises all required components from materialize framework.
 * The be executed on document ready.
 */
Code.materializeJsInit = function() {
  // Navigation bar
  
  $('.button-collapse').sideNav({
      menuWidth: 240,
      activationWidth: 70,
      edge: 'left'
  });
  
  // Drop down menus
  $('.dropdown-button').dropdown({hover: false});
  // Overlay content panels using modals (android dialogs)
  // Pop-up tool tips
  $('.tooltipped').tooltip({'delay': 50});
  // Select menus
  $('select').material_select();
};

/** Binds the event listeners relevant to the page design. */
Code.bindDesignEventListeners = function() {
  // Resize blockly workspace on window resize
  window.addEventListener(
      'resize', Code.resizeBlocklyWorkspace, false);
  // Display/hide the XML load button when the XML collapsible header is clicked
  //document.getElementById('xml_collapsible_header').addEventListener(
  //    'click', Code.buttonLoadXmlCodeDisplay);
};

/** Resizes the container for the Blockly workspace. */
Code.resizeBlocklyWorkspace = function() {
  var contentBlocks = document.getElementById('content_blocks');
  var wrapperPanelSize =
      Code.getBBox_(document.getElementById('blocks_panel'));

  contentBlocks.style.top = wrapperPanelSize.y + 'px';
  contentBlocks.style.left = wrapperPanelSize.x + 'px';
  // Height and width need to be set, read back, then set again to
  // compensate for scrollbars.
  contentBlocks.style.height = wrapperPanelSize.height + 'px';
  contentBlocks.style.height =
      (2 * wrapperPanelSize.height - contentBlocks.offsetHeight) + 'px';
  contentBlocks.style.width = wrapperPanelSize.width + 'px';
  contentBlocks.style.width =
      (2 * wrapperPanelSize.width - contentBlocks.offsetWidth) + 'px';
};

/**
 * Displays or hides the 'load textarea xml' button based on the state of the
 * collapsible 'xml_collapsible_body'.
 */
Code.buttonLoadXmlCodeDisplay = function() {
  var xmlCollapsibleBody = document.getElementById('xml_collapsible_body');
  // Waiting 400 ms to check status due to the animation delay (300 ms)
  setTimeout(function() {
    if (xmlCollapsibleBody.style.display == 'none') {
      $('#button_load_xml').hide();
    } else {
      $('#button_load_xml').fadeIn('slow');
    }
  }, 400);
};

/**
 * Controls the height of the block and collapsible content between 2 states
 * using CSS classes.
 * It's state is dependent on the state of the IDE output collapsible. The
 * collapsible functionality from Materialize framework adds the active class,
 * so this class is consulted to shrink or expand the content height.
 */
Code.contentHeightToggle = function() {
  var outputHeader = document.getElementById('ide_output_collapsible_header');
  var blocks = document.getElementById('blocks_panel');
  var arduino = document.getElementById('content_arduino');
  var xml = document.getElementById('content_xml');

  // Blockly doesn't resize with CSS3 transitions enabled, so do it manually
  var timerId = setInterval(function() {
    window.dispatchEvent(new Event('resize'));
  }, 15);
  setTimeout(function() {
    clearInterval(timerId);
  }, 400);

  // Apart from checking if the output is visible, do not bother to shrink in
  // small screens as the minimum height of the content will kick in and cause
  // the content to be behind the IDE output data anyway.
  if (!outputHeader.className.match('active') && $(window).height() > 800) {
    blocks.className = 'content height_transition blocks_panel_small';
    arduino.className = 'content height_transition content_arduino_small';
    xml.className = 'content height_transition content_xml_small';
  } else {
    blocks.className = 'content height_transition blocks_panel_large';
    arduino.className = 'content height_transition content_arduino_large';
    xml.className = 'content height_transition content_xml_large';
  }

  // If the height transition CSS is left then blockly does not resize
  setTimeout(function() {
    blocks.className = blocks.className.replace('height_transition', '');
    arduino.className = arduino.className.replace('height_transition', '');
    xml.className = xml.className.replace('height_transition', '');
  }, 400);
};

/**
 * Initialises the sketch name input text JavaScript to dynamically adjust its
 * width to the width of its contents.
 */
Code.sketchNameSizeEffect = function() {
  var resizeInput = function() {
    $(this).attr('size', $(this).val().length);
  };

  var correctInput = function() {
    // If nothing in the input, add default name
    if ($(this).val() == '') {
      $(this).val('파일이름');
      $(this).attr('size', 13);
    }
    // Replace all spaces with underscores
    $(this).val($(this).val().replace(/ /g, '_'));
  };

  var sketchNameInput = $('#sketch_name');
  sketchNameInput.keydown(resizeInput).each(resizeInput);
  sketchNameInput.blur(correctInput);
};

/**
 * Sets a string to the SketchName input field and triggers the events set from
 * Ardublockly.sketchNameSizeEffect().
 * @param {string?} newName Optional string to place in the sketch_name input.
 */
Code.sketchNameSet = function(newName) {
  var sketchNewName = newName || '';
  var sketchNameInput = $('#sketch_name');
  sketchNameInput.val(sketchNewName);
  sketchNameInput.attr('size', sketchNewName.length);
  sketchNameInput.keydown();
  sketchNameInput.blur();
};


/**
 * Compute the absolute coordinates and dimensions of an HTML element.
 * @param {!Element} element Element to match.
 * @return {!Object} Contains height, width, x, and y properties.
 * @private
 */
Code.getBBox_ = function(element) {
  var height = element.offsetHeight;
  var width = element.offsetWidth;
  var x = 0;
  var y = 0;
  do {
    x += element.offsetLeft;
    y += element.offsetTop;
    element = element.offsetParent;
  } while (element);
  return {
    height: height,
    width: width,
    x: x,
    y: y
  };
};

/**
 * Sets the text for a "Materialize Modal" (like an android Dialog) to have
 * alert-like HTML messages.
 * @param {!string} title HTML to include in title.
 * @param {!element} body HTML to include in body.
 * @param {boolean=} confirm Indicates if the user is shown and option to just
 *     'Ok' or 'Ok and cancel'.
 * @param {string=|function=} callback If confirm option is selected this would
 *     be the function called when clicked 'OK'.
 */
Code.materialAlert = function(title, body, confirm, callback) {
  $('#gen_alert_title').text(title);
  $('#gen_alert_body').text('');
  $('#gen_alert_body').append(body);
  if (confirm == true) {
    $('#gen_alert_cancel_link').css({'display': 'block'});
    if (callback) {
      $('#gen_alert_ok_link').bind('click', callback);
    }
  } else {
    $('#gen_alert_cancel_link').css({'display': 'none'});
    $('#gen_alert_ok_link').unbind('click');
  }
  $('#gen_alert').openModal();
  window.location.hash = '';
};
