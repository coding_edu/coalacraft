var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require('fs');
var cors = require('cors');

var indexRouter = require('./routes/index');

var app = express();

var beautify = require('js-beautify').js;
var watch = require('node-watch');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header('Access-Control-Allow-Methods', 'POST, GET, DELETE, PUT');
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

const block_path = path.join(__dirname + "/public/js/student-block.js");


fs.truncate(block_path, 0, function(){console.log('delete file content on student-block.js')});

/*
if (fs.existsSync(block_path)) {
  fs.unlinkSync(block_path);
}
*/

/*
fs.readFile(path.join(__dirname + "/public/coala_toolbox.xml"), 'utf8', function(err, data) {
    if(err) throw err;
    var regex = /사용자블록\"\>/g;
    //data = data.replace(regex, "");
    //console.log(data);

});

watch(block_path, { recursive: true }, function(evt, name){
    if(evt == 'update') {
   	fs.readFile(block_path, 'utf8', function (err, data) {
  	  if (err) throw err;
	  var res = data.match(/Blockly.Blocks\[\'\w+\'\]/g);
	    if(data == null) {
		console.log('');
	    } else {
	  	console.log(data);
	    }
	});
    }
});
*/


app.post('/jscode', function(req, res, next) {
    let body = '';
    let ip = req.ip.toString();
    req.on('data', data => {
	
        body += data.toString();
        fs.writeFile(path.join(__dirname + "/../modules/" + ip.replace(/[f:.]/g, "") +".js"), body, function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    }).on('end', () => {
        res.end('ok');
    });
});


app.post('/uploadjs', function(req, res, next) {
    let body = '';
    req.on('data', data => {
	
        body += data.toString();
	body = beautify(body, { indent_size: 2, space_in_empty_paren: true });
	fs.appendFile(block_path, body, function(err) {
 
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        });
    }).on('end', () => {
        res.end('ok');
    });
});

app.get('/uploadj', function(req, res) {
  fs.readFile(block_path, 'utf8', function (err, data) {
    if (err) throw err;
      var regex = data.match(/Blockly.Blocks\[\'\w+\'\]/g);
      res.json(regex);
    });
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
