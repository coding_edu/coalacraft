exports.fireTNT = function(x,y,z,dir,angle,power){
  var a = Number(-power*Math.cos(angle*Math.PI/180)*Math.sin(dir*Math.PI/180)).toFixed(4);
  var b = Number(power*Math.sin(angle*Math.PI/180)).toFixed(4);
  var c = Number(power*Math.cos(angle*Math.PI/180)*Math.cos(dir*Math.PI/180)).toFixed(4);
  var fuse = Number(2*power*Math.sin(angle*Math.PI/180)*20).toFixed(4); // 20 is Correction constant
  var slash = require('slash');
  slash("summon tnt "+x+" "+y+" "+z+" {Motion:["+a+","+b+","+c+"],Fuse:"+fuse+"}");
}
