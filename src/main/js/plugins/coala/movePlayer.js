exports.teleport = function(x, y, z, flag){
  var bkLocation = Packages.org.bukkit.Location;
  if(flag == 'relative'){
    player.teleport(player.getLocation().add(x, y, z));
  } else {
    player.teleport(new bkLocation(player.world, x, y, z));
  }
}

exports.forward = function(distance){
  Location.nforward(player,distance);
}

exports.setDirection = function( yaw ) {
  var bkLocation = org.bukkit.Location;
  var pl = player.getLocation();
  if ( typeof yaw === 'undefined' ){
    if( typeof args[1] === 'undefined'){
      player.sendMessage('Please input a number.');
      return;
    } else {
      yaw = args[1]; // args[1] is exist
    }
  } else {
    // yaw is exist
  }

  if (isNaN(Number(yaw))){
    // yaw is not number
    player.sendMessage('Please input a number.');
    return;
  } else {
    // yaw is number
  }

  player.teleport(new bkLocation(player.world, pl.x, pl.y, pl.z, yaw, 0));
}

exports.useForward = function() {
  if(isNaN(Number(args[1]))){
    player.sendMessage('Please input a number.');
    return false;
  } else {
    Location.nforward(player,args[1]);
  }
}

exports.useTeleport = function() {
  var bkLocation = Packages.org.bukkit.Location;
  if(isNaN(Number(args[1])) || isNaN(Number(args[2])) || isNaN(Number(args[3]))){
    player.sendMessage('Please input a number.');
  } else {
    player.teleport(new bkLocation(player.world, args[1], args[2], args[3]));
    player.sendMessage("Teleport X: "+args[1]+", Y: "+args[2]+", Z: "+args[3]);
  }
}
