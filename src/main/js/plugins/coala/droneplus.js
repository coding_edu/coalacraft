'use strict';
/*global require */
var Drone = require('drone'),
    blocks = require('blocks');
/************************************************************************

### Additional Drone method

***/
function removeXYZ(x1, y1, z1, x2, y2, z2){
	var minX = Math.min(x1,x2);
	var minY = Math.min(y1,y2);
	var minZ = Math.min(z1,z2);
	var maxX = Math.max(x1,x2);
	var maxY = Math.max(y1,y2);
	var maxZ = Math.max(z1,z2);

	this
		.chkpt('rm')
		.move(minX,minY,minZ)
		.setDirection(0)
		.box('0',maxZ-minZ+1,maxY-minY+1,maxX-minX+1)
		.move('rm')

  return this;
}

function removeBlock(width, length, height){
  this.box(0,width,length,height);
  return this;
}

function roof(block, w, d) {
	this.chkpt('roof');
	for(var i = w ; i > 0 ; i -= 2){
		this.box0(block, w, 1, i).up().fwd();
	}

	return this.move('roof');
}

function minihouse(w, l, h) {
  this
  .chkpt('minihouse')
	.down()
	.box ('5',w,1,l) // wood floor
  .move('minihouse')
	.box0('45', w, h, l)  // brick wall
	.move('minihouse')
	.up(h)
	.roof('98',w,l) // stone roof
	.move('minihouse')
	.right(w / 2 - 1)
	.box0('0',2,2,1) // make entrance
	.fwd(l-1)
	.up()
	.box0('102',2,2,1) // back window
	.move('minihouse')
	.fwd(l / 2 - 1)
	.up()
	.box0('102',1,2,2) // left window
	.right(w - 1)
	.box0('102',1,2,2) // right window
	;

	return this.move('minihouse');
}

function spawnMob( entityType , isAdult , saddle ){
  this.up();
  var mob = this.getLocation().world.spawnEntity(this.getLocation(), org.bukkit.entity.EntityType.fromName(entityType));
  if( isAdult ){
    mob.setAdult();
  } else {
    mob.setBaby();
  }

  if( saddle && isAdult && entityType == 'HORSE'){
    try {
      var bkMaterial = Packages.org.bukkit.Material;
      var bkItemStack = Packages.org.bukkit.inventory.ItemStack;
      mob.setTamed(true);
      mob.getInventory().setSaddle(new bkItemStack(bkMaterial.SADDLE));
    } catch(e) {
      player.chat('말 이외에는 안장사용불가');
    }
  }
}

function placeNote( Note ){
  var block = drone.getLocation().getBlock();
  block.setTypeId(25);
  var state = block.getState();
  state.setRawNote(Note%25);
  state.update();
}

function drawArc( blockType, rad , stroke, tl, tr, bl, br, orient, stack, isFill){
  var bm = this.getBlockIdAndMeta(blockType);
  if ( rad < 16 ){
    this.arc({blockType: bm[0],
       meta: bm[1],
       radius: rad,
       strokeWidth: stroke,
       quadrants: { topleft: tl, topright: tr, bottomleft: bl, bottomright: br },
       orientation: orient,
       stack: stack,
       fill: isFill
       });
  } else { // too big
    this.sign(
      ['Build too Big!', 'radius must be', 'less than 16'],
      68
    );
    console.warn('Build arc too big!');
    return this;
  }
  return this;
}

Drone.extend(drawArc);
Drone.extend(placeNote);
Drone.extend(spawnMob);
Drone.extend(minihouse);
Drone.extend(roof);
Drone.extend(removeBlock);
Drone.extend(removeXYZ);
