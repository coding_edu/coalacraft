events.entityDeath( function (event) {
    var player = event.getEntity().getKiller();
    var host = player.getAddress().getAddress().getHostAddress().replace(/[:.]/gi,'')
    var entity = event.getEntity().getType();
    global.player = player;
    global.event = event;
    global.drone = new Drone(player);

    try {
      var user = require(host, true)
    }
    catch(e) {
      // The player does not have a code file.
    }

    try {
      eval('user.on'+entity+'Death()');
    }
    catch(e) {
      // Player did not create a function.
    }
});
