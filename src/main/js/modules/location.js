'use strict';
/*global require */
/************************************************************************
### Location object
Make new javascript object location

by Coala
***/
function absolute(player, x, y, z, pitch, yaw ) {
	var bkLocation = org.bukkit.Location;
	var pl = player.getLocation();
	pitch = typeof pitch !== 'undefined' ? pitch : pl.pitch;
	yaw = typeof yaw !== 'undefined' ? yaw : pl.yaw;
    return new bkLocation(player.world, Math.floor(x)+0.5, Math.floor(y), Math.floor(z)+0.5, yaw, pitch);
}

function relative(player, x, y, z) {
	return player.getLocation().add(x,y,z);
}

function nforward(player, d) {
	var yaw = ((player.getLocation().getYaw() + 90) * Math.PI) / 180;
	var nl = player.getLocation().add(Math.cos(yaw)*d, 0, Math.sin(yaw)*d);
	player.teleport(nl);
}

function setyaw(player, yaw) {
	var bkLocation = org.bukkit.Location;
	var pl = player.getLocation();
	yaw = typeof yaw !== 'undefined' ? yaw : pl.yaw;
	var nl = new bkLocation(player.world, pl.x, pl.y, pl.z, yaw, 0);
	player.teleport(nl);
}

exports.relative = relative;
exports.absolute = absolute;
exports.nforward = nforward;
exports.setyaw = setyaw;