'use strict';
/*
  execute a JSP command.
*/

exports.exec = function( args, player ) {
  if ( args.length === 0 ) {
    throw new Error('Usage: jsp command-name command-parameters');
  }

  var name = args[0]
  var host = player.getAddress().getAddress().getHostAddress().replace(/[:.]/gi,'')
  var result = null

  global.args = args;
  global.player = player;
  global.drone = new Drone(player);

  try {
    var user = require(host, false);
  }
  catch(e) {
    player.sendMessage('No file saved');
    //throw new Error('No file saved');
  }

  try {
    eval('user.'+name+'()');
    //eval('user.'+name+'(player)');
  }
  catch(e) {
    player.sendMessage('function error: ' + name);
    //throw new Error('No function defined: dddd' + name);
  }

  return result
}
