package org.scriptcraftjs.bukkit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.Paths;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

public class ScriptCraftPlugin extends JavaPlugin implements Listener
{
    public boolean canary = false;
    public boolean bukkit = true;
    // right now all ops share the same JS context/scope
    // need to look at possibly having context/scope per operator
    //protected Map<CommandSender,ScriptCraftEvaluator> playerContexts = new HashMap<CommandSender,ScriptCraftEvaluator>();
    private String NO_JAVASCRIPT_MESSAGE = "No JavaScript Engine available. ScriptCraft will not work without Javascript.";
    protected ScriptEngine engine = null;
    
	private static final String ALGO = "AES";
	private static final byte[] keyValue =
			new byte[]{'C', 'o', 'a', 'l', 'a', 'E', 'x', 'p', 'i', 'r', 'y', 'D', 'a', 't', 'e', 'e'};

//    ProcessBuilder pb = new ProcessBuilder("node", "scriptcraft/www/bin/www");
//    Process process;

    @Override public void onEnable()
    {
    	Thread currentThread = Thread.currentThread();
        ClassLoader previousClassLoader = currentThread.getContextClassLoader();
        currentThread.setContextClassLoader(getClassLoader());
        try {
            ScriptEngineManager factory = new ScriptEngineManager();
            this.engine = factory.getEngineByName("JavaScript");
			if (this.engine == null) {
				this.getLogger().severe(NO_JAVASCRIPT_MESSAGE);
			} else {
				Invocable inv = (Invocable) this.engine;
				this.engine.eval(new InputStreamReader(this.getResource("boot.js")));
				inv.invokeFunction("__scboot", this, engine);
			}

            //process = pb.start();
            //this.getLogger().info("Node.js webserver started");
            //this.getLogger().info("Webserver URL - http://" + getHost4Address() + "/");
            
        	if (validateExpireDate()) {
        		this.getLogger().info("Coalacraft plugin is activated.");
        	} else {
        		this.getLogger().warning("Your license has expired. Please contact webmaster@coalasw.com");
        		Bukkit.getPluginManager().disablePlugin(this);
        	}
		} catch (Exception e) {
			e.printStackTrace();
			this.getLogger().severe(e.getMessage());
		} finally {
			currentThread.setContextClassLoader(previousClassLoader);
		} 	
    	
    }


    @Override public void onDisable() {
        super.onDisable();
        //process.destroy();
        this.getLogger().info("Node.js webserver stopped");
    }

    
    public List<String> onTabComplete(CommandSender sender, Command cmd,
                                      String alias,
                                      String[] args)
    {
        List<String> result = new ArrayList<String>();
        if (this.engine == null) {
            this.getLogger().severe(NO_JAVASCRIPT_MESSAGE);
            return null;
        }
        try {
            Invocable inv = (Invocable)this.engine;
            inv.invokeFunction("__onTabComplete", result, sender, cmd, alias, args);
        } catch (Exception e) {
            sender.sendMessage(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        boolean result = false;
        Object jsResult = null;
        if (this.engine == null) {
            this.getLogger().severe(NO_JAVASCRIPT_MESSAGE);
            return false;
        }
        try {
            jsResult = ((Invocable)this.engine).invokeFunction("__onCommand", sender, cmd, label, args);
        } catch (Exception se) {
            this.getLogger().severe(se.toString());
            se.printStackTrace();
            sender.sendMessage(se.getMessage());
        }
        if (jsResult != null){
            return ((Boolean)jsResult).booleanValue();
        }
        
        if (cmd.getName().equalsIgnoreCase("start")) {
            Bukkit.broadcastMessage("Started");
            BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
            scheduler.scheduleSyncDelayedTask(this, new Runnable() {
                @Override
                public void run() {
                    Bukkit.broadcastMessage("Stopped");
                }
            }, 100);

        }
        
        return result;
    }
    
    /**
     * Returns this host's non-loopback IPv4 addresses.
     * 
     * @return
     * @throws SocketException 
     */
    private static List<Inet4Address> getInet4Addresses() throws SocketException {
        List<Inet4Address> ret = new ArrayList<Inet4Address>();

        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
        for (NetworkInterface netint : Collections.list(nets)) {
            Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
            for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                if (inetAddress instanceof Inet4Address && !inetAddress.isLoopbackAddress()) {
                    ret.add((Inet4Address)inetAddress);
                }
            }
        }

        return ret;
    }

    /**
     * Returns this host's first non-loopback IPv4 address string in textual
     * representation.
     * 
     * @return
     * @throws SocketException
     */
    private static String getHost4Address() throws SocketException {
        List<Inet4Address> inet4 = getInet4Addresses();
        return !inet4.isEmpty()
                ? inet4.get(0).getHostAddress()
                : null;
    }
    
    /**
     * Decrypt a string with AES algorithm.
     *
     * @param encryptedData is a string
     * @return the decrypted string
     */
    private static String decrypt(String encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Base64.getDecoder().decode(encryptedData);
        byte[] decValue = c.doFinal(decordedValue);
        return new String(decValue);
    }
    
    /**
     * Generate a new encryption key.
     */
    private static Key generateKey() throws Exception {
        return new SecretKeySpec(keyValue, ALGO);
    }
    
    /**
     * Validate expire date.
     */
    private static boolean validateExpireDate() {
		try {
			Date date = new Date();
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");	
			
			String encryptedData = readLicenseFile();
			
			if(encryptedData == null) {
				return false;
			} else {
				String decryptedData = decrypt(encryptedData);
				if(decryptedData.substring(0, 5).equalsIgnoreCase("alaoc") &&
						decryptedData.substring(decryptedData.length()-3, decryptedData.length()).equalsIgnoreCase("wws")) {
					int currentDate = Integer.parseInt(df.format(date));
					int expiryDate = Integer.parseInt(decryptedData.substring(5, 13));
					
					if (currentDate <= expiryDate) {
						return true;
					} else {
						return false;
					}
				}
				else {
					return false;
				}
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return false;
    }
    
	/**
	 * Read coalacraft license file.
	 */
	private static String readLicenseFile() {
		try {
	        BufferedReader br = new BufferedReader(new FileReader(Paths.get(Bukkit.getWorldContainer().toString(), "license", "LICENSE").toString()));
	        while(true) {
	            String line = br.readLine();
	            if (line==null) break;
	            br.close();
	            return line;       
	        }
	        br.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
}
