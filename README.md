# CoalaSW Minecraft Block Coding for Local Server

선생님들의 로컬PC를 이용한 마인크래프트 **게임서버** 및 **블록작업공간** 제공을 위한 프로젝트 입니다.

scriptcraft plugin에 Nodejs webserver를 결합한 플러그인입니다.

Nodejs 설치를 필요로 합니다. https://nodejs.org/ko/
